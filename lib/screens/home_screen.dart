import 'package:FashionApp/screens/cart_screen.dart';
import 'package:FashionApp/widgets/horizontal_listview.dart';
import 'package:FashionApp/widgets/products_list.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget listTile(String name, Icon icon) {
    return ListTile(
      title: Text(name),
      leading: icon,
      onTap: () {},
    );
  }

  Widget imageCarousel = Container(
    height: 200.0,
    child: Carousel(
      boxFit: BoxFit.cover,
      images: [
        AssetImage('images/m1.jpeg'),
        AssetImage('images/m2.jpg'),
        AssetImage('images/w4.jpeg'),
        AssetImage('images/c1.jpg'),
        AssetImage('images/w3.jpeg'),
      ],
      autoplay: true,
      animationCurve: Curves.fastOutSlowIn,
      animationDuration: Duration(milliseconds: 1000),
      indicatorBgPadding: 6.0,
      dotSize: 6,
      dotBgColor: Colors.transparent,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: Colors.red,
        title: Text('FashApp'),
        actions: [
          IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => Cart())),
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text('Hritik'),
              accountEmail: Text('krishIndia@gmail.com'),
              currentAccountPicture: GestureDetector(
                child: CircleAvatar(
                  backgroundImage: AssetImage('images/sai tej.jpg'),
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.grey[400],
              ),
            ),
            ListTile(
              title: Text('Home'),
              leading: Icon(
                Icons.home,
                color: Colors.red,
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              title: Text('MyAccount'),
              leading: Icon(
                Icons.person,
                color: Colors.red,
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text('Orders'),
              leading: Icon(
                Icons.shopping_basket,
                color: Colors.red,
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text('Shoping Cart'),
              leading: Icon(
                Icons.shopping_cart,
                color: Colors.red,
              ),
              onTap: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Cart())),
            ),
            ListTile(
              title: Text('Favourites'),
              leading: Icon(
                Icons.favorite,
                color: Colors.red,
              ),
              onTap: () {},
            ),
            Divider(),
            ListTile(
              title: Text('Settings'),
              leading: Icon(
                Icons.settings,
                color: Colors.blueGrey,
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text('About'),
              leading: Icon(
                Icons.help,
                color: Colors.blue,
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
      body: ListView(
        children: [
          imageCarousel,
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
            child: Text(
              'Categories',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          HorizontalList(),
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              'Recent Products',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            height: 350,
            child: Products(),
          )
        ],
      ),
    );
  }
}
