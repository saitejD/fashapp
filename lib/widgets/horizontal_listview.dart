import 'package:flutter/material.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Catergory(
            image_location: 'images/cats/tshirt.png',
            image_caption: 'tshirt',
          ),
          Catergory(
            image_location: 'images/cats/dress.png',
            image_caption: 'dress',
          ),
          Catergory(
            image_location: 'images/cats/informal.png',
            image_caption: 'informal',
          ),
          Catergory(
            image_location: 'images/cats/jeans.png',
            image_caption: 'pants',
          ),
          Catergory(
            image_location: 'images/cats/shoe.png',
            image_caption: 'shoes',
          ),
          Catergory(
            image_location: 'images/cats/accessories.png',
            image_caption: 'accessories',
          ),
        ],
      ),
    );
  }
}

class Catergory extends StatelessWidget {
  final String image_location;
  final String image_caption;

  const Catergory({Key key, this.image_location, this.image_caption})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(2.0),
      child: InkWell(
        onTap: () {},
        child: Container(
          width: 100,
          child: ListTile(
              title: Image.asset(
                image_location,
                width: 100,
                height: 60,
              ),
              subtitle: Container(
                alignment: Alignment.topCenter,
                child: Text(
                  image_caption,
                ),
              )),
        ),
      ),
    );
  }
}
