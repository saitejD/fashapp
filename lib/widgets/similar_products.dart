import 'package:FashionApp/widgets/products_list.dart';
import 'package:flutter/material.dart';

class SimilarProducts extends StatefulWidget {
  @override
  _SimilarProductsState createState() => _SimilarProductsState();
}

class _SimilarProductsState extends State<SimilarProducts> {
  var productList = [
    {
      "name": "Blazer",
      "picture": "images/products/blazer1.jpeg",
      "old_price": 120,
      "price": 85,
    },
    {
      "name": "Torn Jeans",
      "picture": "images/products/pants2.jpeg",
      "old_price": 40,
      "price": 37,
    },
    {
      "name": "Red dress",
      "picture": "images/products/dress1.jpeg",
      "old_price": 100,
      "price": 50,
    },
    {
      "name": "Blazer Women",
      "picture": "images/products/blazer2.jpeg",
      "old_price": 60,
      "price": 45,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: productList.length,
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return SingleProduct(
            prod_name: productList[index]['name'],
            prod_picture: productList[index]['picture'],
            prod_old_price: productList[index]['old_price'],
            prod_price: productList[index]['price'],
          );
        });
  }
}
