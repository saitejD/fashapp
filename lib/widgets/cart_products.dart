import 'package:flutter/material.dart';

class CartProducts extends StatefulWidget {
  @override
  _CartProductsState createState() => _CartProductsState();
}

class _CartProductsState extends State<CartProducts> {
  var productsOnCart = [
    {
      "name": "Blazer",
      "picture": "images/products/blazer1.jpeg",
      "price": 85,
      "size": "M",
      "color": "Black",
      "quantity": 1,
    },
    {
      "name": "Torn Jeans",
      "picture": "images/products/pants2.jpeg",
      "price": 37,
      "size": "S",
      "color": "Red",
      "quantity": 3,
    },
  ];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: productsOnCart.length,
        itemBuilder: (context, index) {
          return SingleCartProduct(
            cart_prod_name: productsOnCart[index]["name"],
            cart_prod_price: productsOnCart[index]["price"],
            cart_prod_picture: productsOnCart[index]["picture"],
            cart_prod_color: productsOnCart[index]["color"],
            cart_prod_qty: productsOnCart[index]["quantity"],
            cart_prod_size: productsOnCart[index]['size'],
          );
        });
  }
}

class SingleCartProduct extends StatelessWidget {
  final cart_prod_name;
  final cart_prod_picture;
  final cart_prod_price;
  final cart_prod_size;
  final cart_prod_color;
  final cart_prod_qty;

  const SingleCartProduct(
      {Key key,
      this.cart_prod_name,
      this.cart_prod_picture,
      this.cart_prod_price,
      this.cart_prod_size,
      this.cart_prod_color,
      this.cart_prod_qty})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Image.asset(
          cart_prod_picture,
          width: 80,
          height: 100,
          fit: BoxFit.fill,
        ),
        title: Text(cart_prod_name),
        subtitle: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(0),
                  child: Text("Size:"),
                ),
                Padding(
                  padding: EdgeInsets.all(4),
                  child: Text(
                    cart_prod_size,
                    style: TextStyle(color: Colors.red),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 8, 8, 8),
                  child: Text("Color:"),
                ),
                Padding(
                  padding: EdgeInsets.all(0),
                  child: Text(cart_prod_color,
                      style: TextStyle(color: Colors.red)),
                ),
              ],
            ),
            Container(
              alignment: Alignment.topLeft,
              child: Text(
                "\$${cart_prod_price}",
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
