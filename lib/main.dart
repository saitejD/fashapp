import 'package:flutter/material.dart';
import 'package:FashionApp/screens/export.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      title: 'FashApp',
    ),
  );
}
