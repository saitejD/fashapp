# FashionApp

It is a e-commerce shopping app UI. Where you can see a decent look of shopping site with minimal implementation of features.

# App UI

![screen1](/uploads/47143a3e38692f1d33057f46b8688e11/screen1.jpg)

![screen2](/uploads/4a76f0b3395ecc7cd184b4f6112b8dcb/screen2.jpg)

![screen3](/uploads/c89ec1cbddbf19d51a86c9648125fbe8/screen3.jpg)
